﻿using System;
using System.Net.Sockets;
using System.Windows;
using System.IO;


namespace WpfApplication23sdfd
{
    class Program
    {

    static StreamWriter writeImageData;
    static NetworkStream nStream;
    static string Base64ImageData;
    static string BlockData;
    static int RemainingStringLength = 0;
    static bool Done = false;

    static void Main(string[] args)
    {
        try
        {
            TcpClient tcpClient = new TcpClient("192.168.43.211", 1820);
            nStream = tcpClient.GetStream();
            writeImageData = new StreamWriter(nStream);

            //Change the filename here. If you change the file type
            FileStream fs = File.OpenRead("namafile");
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, ImageData.Length);
            Base64ImageData = Convert.ToBase64String(ImageData);
            int startIndex = 0;
            Console.WriteLine("Transfering Data...");
            while (Done == false)
            {
                while (startIndex<Base64ImageData.Length)
                {
                    try
                    {
                        BlockData = Base64ImageData.Substring(startIndex, 100);
                        writeImageData.WriteLine(BlockData);
                        writeImageData.Flush();
                        startIndex += 100;
                    }
                    catch (Exception er)
                    {
                        RemainingStringLength = Base64ImageData.Length - startIndex;
                        BlockData = Base64ImageData.Substring(startIndex, RemainingStringLength);
                        writeImageData.WriteLine(BlockData);
                        writeImageData.Flush();
                        Done = true;
                        break;
                    }
                }
            }

            writeImageData.Close();
            tcpClient.Close();

            Console.WriteLine("Transfer Complete");
        }
        catch (Exception er)
        {
            Console.WriteLine("Unable to connect to server");
            Console.WriteLine(er.Message);
        }
        Console.ReadKey();
        }
    }

}